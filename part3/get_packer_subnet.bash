#!/bin/bash
# https://docs.aws.amazon.com/AWSEC2/latest/APIReference/API_DescribeSubnets.html
# Based off a python example here:
# https://docs.aws.amazon.com/general/latest/gr/sigv4-signed-request-examples.html

# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY need to be passed in via the environment.
if [[ -z "$AWS_ACCESS_KEY_ID" ]] ; then
    (>&2 echo "AWS_ACCESS_KEY_ID needs to be defined for the script to work.")
    exit 1
fi

if [[ -z "$AWS_SECRET_ACCESS_KEY" ]] ; then
    (>&2 echo "AWS_SECRET_ACCESS_KEY needs to be defined for the script to work.")
    exit 1
fi

subnet_name="$1"

# The most important part. This is what we're doing via the API.
# %3A is a :
action="DescribeSubnets&Filter.1.Name=tag%3AName&Filter.1.Value.1=${subnet_name}"

# We need these datestamps for the request and signing key.
amz_date="$(date +%Y%m%dT%H%M%SZ)"
datestamp="$(date +%G%m%d)"

# We need these static values for the request and signing key.
# Copied from the python script!
# Don't ask me why these need to be the values they are.
api_version="2016-11-15"
hashing_alg="AWS4-HMAC-SHA256"
expiration="30"
signed_headers="host"
region="us-east-1"
service="ec2"
request_version="aws4_request"
method="GET"
canonical_uri="/"
host="${service}.amazonaws.com"

# This is a method of assigning a multi line string to variable in bash.
# It's pretty unintuitive. Sorry about that.
canonical_headers="host:${host}"$'\n'

# First we build the "X-Amz-Credential" field.
# We need to use %2F instead of / for reasons.
credential_glob="${AWS_ACCESS_KEY_ID}%2F${datestamp}%2F${region}%2F${service}%2F${request_version}"

# Then we build a "canonical querystring" whatever that is.
canonical_querystring="Action=${action}&\
Version=${api_version}&\
X-Amz-Algorithm=${hashing_alg}&\
X-Amz-Credential=${credential_glob}&\
X-Amz-Date=${amz_date}&\
X-Amz-Expires=${expiration}&\
X-Amz-SignedHeaders=${signed_headers}"

# After that we need to calculate a payload hash.
payload_hash=$(echo -n "" | openssl dgst -sha256 -binary | xxd -p -c 32)

# Next we need to build the canonical request.
# This is ANOTHER method of assigning a multi line string to variable in bash.
# It's pretty unintuitive. Sorry about that. Bash is weird.
read -r -d "" canonical_request <<EOF
${method}
${canonical_uri}
${canonical_querystring}
${canonical_headers}
${signed_headers}
${payload_hash}
EOF

# Now that we have our canonical request, we need to make a hexdigest of it.
# This doesn't make any sense I know!
canonical_request_digest=$(echo -n "$canonical_request" | openssl dgst -sha256 -binary | xxd -p -c 32)

# Before we can build the string to sign, we need to build the credential scope.
# This is different from the credential glob that we used earlier!
credential_scope="${datestamp}/${region}/${service}/${request_version}"

# We can FINALLY build the string to sign.
# We need to build this using another one of those multi line variable things.
# Also many things are repeating, not really sure why but that's how it is.
read -r -d "" string_to_sign <<EOF
${hashing_alg}
${amz_date}
${credential_scope}
${canonical_request_digest}
EOF

# This builds our signing key using our AWS secret key.
kdate=$(echo -n "$datestamp" | openssl dgst -sha256 -binary -hmac "AWS4${AWS_SECRET_ACCESS_KEY}" | xxd -p -c 32)
# We need to pass the hmac in in hexadecimal because bash strings can't ever contain nulls.
kregion=$(echo -n "$region" | openssl dgst -sha256 -binary -mac HMAC -macopt "hexkey:${kdate}" | xxd -p -c 32)
kservice=$(echo -n "$service" | openssl dgst -sha256 -binary -mac HMAC -macopt "hexkey:${kregion}" | xxd -p -c 32)
ksigning=$(echo -n "$request_version" | openssl dgst -sha256 -binary -mac HMAC -macopt "hexkey:${kservice}" | xxd -p -c 32)

# We sign our string to sign using our signing key and output as a hexdigest.
# This is our signature!
signature=$(echo -n "$string_to_sign" | openssl dgst -sha256 -binary -mac HMAC -macopt "hexkey:${ksigning}" | xxd -p -c 32)

# We built the final querystring from the canonical querystring and our signature.
result_querystring="${canonical_querystring}&X-Amz-Signature=${signature}"

# This is a working url! 
result_url="https://${host}${canonical_uri}?${result_querystring}"

wget -q -O - "$result_url" | sed -n 's/\s*<subnetId>\(.*\)<\/subnetId>/\1/gp'
