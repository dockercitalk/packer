#!/bin/bash
set -e

# First we install docker-ce using the instructions from their website.
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common tmux
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce

# Second we install gitlab-runner using the instructions from their website.
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo chmod 644 /tmp/pin-gitlab-runner.pref
sudo chown root:root /tmp/pin-gitlab-runner.pref
sudo mv /tmp/pin-gitlab-runner.pref /etc/apt/preferences.d/pin-gitlab-runner.pref
sudo apt-get install -y gitlab-runner

# Third we install and enable the gitlab-runner registration service.
# We replace __RUNNER_TOKEN__ with the real runner token via sed.
sed -i "s/__RUNNER_TOKEN__/${RUNNER_TOKEN}/" /tmp/register-gitlab.service
# Fix the file permissions then move it.
sudo chmod 644 /tmp/register-gitlab.service
sudo chown root:root /tmp/register-gitlab.service
sudo mv /tmp/register-gitlab.service /etc/systemd/system/register-gitlab.service
sudo systemctl enable register-gitlab

# Last we clean up the /tmp directory.
sudo rm -rf /tmp/*
